
# Bedrock

### A Wordpress starter theme forked from [digisavvy/some-like-it-neat](https://github.com/digisavvy/some-like-it-neat). Built specifically for the needs of Side Porch. Built to be used with VCCW.

Description
-----------

Bedrock is a minimal starter theme that is responsive out of the box. It uses Sass along with Bourbon Neat for help with responsive grids. It's based on Underscores. Bedrock is meant to be paired with [VCCW](https://github.com/vccw-team/vccw) to create a portable, automated development environment. Less config, more getting to work.

__Note: This README is in flux. I'm in the process of converting the public project README into something more suitable for us internally.__

Getting Started (Short version)
-------------------------------
1.

2.

3.

4.

What's Inside?
---------------

* Underscores (s) based theme. There's smarter folks than me building great shit (http://underscores.me)

* Sass. We're using it and to update this theme you should be cozy with it or get ready to learn how to use. If you don't know Sass, you should definitely jump in. The water's fine and you'll thank me later. I accept thanks in burritos, doritos, fritos and cheetos only.

* Bourbon (http://bourbon.io), Neat (http://neat.bourbon.io), Bitters (http://bitters.bourbon.io) and Refills (http://refills.bourbon.io)

  * Bourbon provides a lightweight Sass library (similar to Compass).

  * Neat extends bourbon and provides a nice and lightweight grid framework as a base for this project. 
  
  * Refills are plug and play components built with Bourbon and Neat. Accordians, cards, footers, navigations, tables, and a few other pre-built front-end pieces.  

  * Bitters are scaffold styles, variables and structure for Bourbon projects 

* Theme Hook Alliance — One of the things I learned to love about working with Frameworks were their hooks. Thematic and Genesis introduced me to the notion. Since them I've been using them like they're going out of style. When I set out to make my own starter theme I wanted to make something that had "just the right amount" of features for me. I knew I needed hooks. The THA project was intro'd to me by Brandon Dove, at the OCWP (http://ocwp.org) developer's day meetup. Thought it was super neat. So I bundled that hot mess right into this thing.

* Gulpjs Task Automation — This has been a biggy! Lots of work done and yet to-do. Your gulpjs file will help automate nifty
tasks such as autoprefixing, compiling and minifying Sass files; cleaning up your theme directory and even packaging/zipping
your theme! Cool. Right?

* Bower — We're using bower to manage certain theme dependencies, specifically Bourbon, Neat and Bi-App-Sass. Cool, right? That's what I said when I learned the awesomeness that is Bower. Ya hurd?!

* Composer — This one is a bit

* RTL Support via the most excellent bi-app-sass project
* PHPCS w/ WordPress Coding Standards
* Built for Accessibility
* Flexnav Menu System and Hover Intent
* TGM PLugin Activation
* Bower for managing packages and dependencies
* Infinite Scroll Support for Jetpack

Getting Started (Long version from original README)
---------------

There are things you need to install before you hack away at things. There are three package managers to install: Node (which installs the NPM package manager), Bower and Composer. Each of these have dependencies that also need to be installed. Fortunately, this is all "fairly easy".

* #### Prerequisites
  * You'll need to download and install [Node](https://nodejs.org/)
  * You will also need to download and install [Sass](http://sass-lang.com/install)

* #### Getting and Installing the Theme
  * The first thing you’ll want to do is grab a copy of the theme —
**git clone git://github.com/digisavvy/some-like-it-neat.git** – or [download](http://github.com/digisavvy/some-like-it-neat) it and then rename the directory to the name of your theme or website.


* #### Install Gulpjs, Composer and Bower + Dependencies

  Once you have Node, Sass and the theme installed, the next step is simple enough.

   **_(note - you may have to run the following commands as admin or sudo)_**

  * **Install Gulp** — Open a command prompt/terminal and navigate to your theme's root directory and run this command: `npm install` - This installs all the necessary Gulp plugins to help with task automation such as Sass compiling and browser-sync! You'll need to run this step on each of your projects, going forward.
  * **Install Bower** - In the command prompt/terminal run this command: `npm install -g bower`. This installs Bower (the -g flag installs globally, not just in the current directory, super friends). Your only need to do this step once.
  * **Install Bower Dependencies** - There are Sass and Js packages that are required by Some Like it Neat. To get them run this command: `bower install` - This will install the theme's dependencies such as bourbon, neat, flexnav etc.

  * **Install Composer** - Install Composer, Globally, so that it will be available in your other projects, too! You only have to do this part once.
    	* [Install Composer](https://getcomposer.org/doc/00-intro.md#globally)

  * **Install Composer Dependencies** - The only Composer Dependency setup at this time is PHPCS w/ WordPress Coding Standards. This allows us to run a Gulp task to "sniff" out any code in your theme that isn't standards-compliant. You can then go back and fix the offending code. The Phpcs task in gulpfile.js can be configured to included/exclude any directory within your theme. By default, it looks at php files in theme-root and files within the page-templates directory.

  	* Once you have completed the Composer install, you should be able to run the following command to install PHPCS: `php composer.phar install`.
  	* You can update the composer file via `sudo composer self-update` command.


 * #### Set your project configuration in Gulpfile.js!!
_Be sure to go into gulpfile.js and setup the project configuration variables._

 	* This is important for using Browser-Sync with your project. Make sure in gulpfile.js that you set the `project` variable to the appropriate name for your project URL. Default is "yourlocal.dev"

 * #### Generating your styles
   * In pre 1.1.11 builds of Some Like it Neat, Style.scss would process/compile all of your changes to the various Sass files. This has changed in 1.1.11. We have added rtl
   support using a set of mixins from the Bi-App-Sass [view](http://anasnakawa.github.io/bi-app-sass/) project which helps us generate styles for RTL configurations. All LTR styles are output to style.css and RTL styles are output to rtl.css.

* #### Gulp Tasks
There are a couple of tasks built into Some Like it Neat to help get you going.
  * `gulp` This command simply starts up Gulp and watches your scss, js and php filder for changes, writes them out and refreshes the browser for you.
  * `gulp build` This command removes unneccessary files and packs up the required files into a nice and neat, installable, zip package. Honestly, this is here because I was uploading my theme to the WP.org uploader so many times... Epitome of the laze.

Each task such as 'js', 'images' or 'browser-sync' may be started individually. Although, the only one of them you'd do that with is the 'images' task since that's not auto-optimizing at the moment.

* #### Theme Development, Minification and You
When developing your theme note that the output style.css file and production.js file are in expanded (readable) format if WP_DEBUG is set to true in wp-config.php. If WP_DEBUG is NOT set to true, then style.css and production.js are minified for you. While developing your theme, I recommend that WP_DEBUG is set to true. Just a good practice anyway.

* **A Note About Javascript Files** - If you have JS files that are not managed by Bower, you should place those files inside the assets/js/app folder. Why? Gulp runs a task that concatenates js files in that directory and checks them for errors, which is pretty nifty. You can modify Gulp task behavior to suit your tastes, of course.

* **Extra Note!** If you've set WP Debug true, the concatenated file is unminified and if set to false, then the concatenated file is minified. If you don't intend to use this functionality, you should comment-out or remove the lines referring to development.js and production-min.js.

### Theme Hook Alliance
---------------

What is Theme Hook Alliance? It's a pretty rad project - https://github.com/zamoose/themehookalliance. I'm a big fan of hooks, personally. They provide a means to keep things within the theme cleaner and easier to maintain.

"_The Theme Hook Alliance is a community-driven effort to agree on a set of third-party action hooks that THA themes pledge to implement in order to give that desired consistency_."

### Infinite Scroll and Jetpack
---------------
Infinite Scroll is now supported. Requires that you have Jetpack installed and have configured the plugin to use Infinite Scroll. Additionally, you'll need to go into the Customizer to add theme support. Why? While redundant, didn't really want code running that wasn't in use... Aaand seemed like a nice use of the Customizer. If you hate it, open an issue. =)

Road Map
---------------
#### Side Porch road map

#### From old readme
* Firm up i18n for RTL and language support. Looking for contributors here
* Ensure Accessibility has been properly and thoroughly addressed
* Implement Yeoman
* <s>Get Bower properly setup and configured</s>

### Folder Structure
---------------
I haven't listed out every single file here; but I have listed out the files that you'll most likely work with during
a project.

<pre style="max-height: 300px;"><code>Theme Root
    │    ├── assets
    │    │   ├── bower_components
    │    │   |   ├── bi-app-sass
    │    │   |   ├── bourbon
    │    │   |   └── neat
    │    │   ├── css
    |    |   |    ├── rtl-min.css
    |    |   |    ├── rtl.css
    |    |   |    ├── style-min.css
    |    |   |    └── style.css
    │    |   └──  js
    │    │   |    ├── app
    │    │   |    └── vendor
    │    |   |    |   ├── flexnav
    │    |   |    |   ├── hoverintent
    │    |   |    |   ├── modernizr
    │    |   |    |   └── selectivizr
    │    │   |    ├── production-min.js
    │    │   |    └── development.js
    │    ├── sass
    │    |   └── base (Bitters)
    │    |   └── components
    |    |    |   ├── _buttons.scss
    |    |    |   ├── _dashicons.scss
    |    |    |   ├── _flexnav.scss
    |    |    |   ├── _navigation.scss
    |    |    |   ├── _ui-bourbon.scss
    |    |    |   └── _variables.scss
    │    |    └── layouts
    |    |    |   ├── _content.scss
    |    |    |   ├── _footer.scss
    |    |    |   ├── _header.scss
    |    |    |   ├── _navigation.scss
    |    |    |   ├── _normalize.scss
    |    |    |   ├── _sidebar.scss
    |    |    |   ├── _structure.scss
    |    |    |   └── _typography.scss
    |    ├── _app.scss
    |    ├── _grid-settings.scss
    |    ├── _rtl.scss
    |    └── style.scss
    ├── library
    │   └── languages
    │   │   ├── some_like_it_neat.pot
    │   └── vendors
    │   │   ├── js
    │   │   ├── tgm-plugin-activation
    │   │   ├── tha-theme-hooks
    │   │   └── customizer
    │   ├── custom-header.php
    │   ├── extras.php
    │   ├── jetpack.php
    │   └── template-tags.php
    ├── page-templates
    │     └── partials
    |     |   ├── content-aside.php
    |     |   ├── content-audio.php
    |     |   ├── content-chat.php
    |     |   ├── content-gallery.php
    |     |   ├── content-image.php
    |     |   ├── content-link.php
    |     |   ├── content-none.php
    |     |   ├── content-page.php
    |     |   ├── content-quote.php
    |     |   ├── content-single.php
    |     |   ├── content-status.php
    |     |   ├── content-video.php
    |     |   └── content.php
    |     ├── template-full-width.php
    |     ├── template-left-col.php
    |     └── template-right-col.php
    ├── .bowerrc
    ├── 404.php
    ├── archive.php
    ├── comments.php
    ├── footer.php
    ├── functions.php
    ├── gulpfile.js
    ├── header.php
    ├── index.php
    ├── license.txt
    ├── package.json
    ├── page.php
    ├── README.md
    ├── rtl.css
    ├── search.png
    ├── searchform.php
    ├── sidebar.php
    ├── single.php
    └── style.css</code></pre>


